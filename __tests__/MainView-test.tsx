import 'react-native';
import React from 'react';
import App from '../src/App';

import axios from 'axios';
import {render, fireEvent} from '@testing-library/react-native';
import {ApiDataResponse} from '../src/store/types/apiDataTypes';
import Config from '../src/config';
import {act} from 'react-test-renderer';

jest.mock('axios');

const mockResponse: ApiDataResponse = {
  data: {
    list: [
      {
        main: {
          temp: 23,
          feels_like: 698,
        },
        weather: [
          {
            description: 'not bad',
          },
        ],
        dt: 1630237235,
      },
    ],
  },
};
const axiosSpy = jest.spyOn(axios, 'get');

describe('MainView', () => {
  afterEach(() => axiosSpy.mockClear());

  test('should render items on success', async () => {
    axiosSpy.mockImplementation(() => Promise.resolve(mockResponse));
    const {getByTestId, getAllByTestId} = render(<App />);

    const input = getByTestId('input');
    const button = getByTestId('button');
    await act(async () => {
      await fireEvent.changeText(input, 'Berlin');
      await fireEvent.press(button);
    });

    const items = getAllByTestId('list-item');

    expect(input).toBeTruthy();
    expect(button).toBeTruthy();
    expect(items).toHaveLength(1);
  });

  test('should call correct endpoint on button press', async () => {
    const {getByTestId} = render(<App />);

    const input = getByTestId('input');
    const button = getByTestId('button');

    await act(async () => {
      await fireEvent.changeText(input, 'Berlin');
      await fireEvent.press(button);
    });

    expect(input).toBeTruthy();
    expect(button).toBeTruthy();
    expect(axiosSpy).toHaveBeenCalledWith(`${Config.API_URL}Berlin,de`);
  });

  test('should show error component on api error', async () => {
    axiosSpy.mockImplementation(() => Promise.reject(new Error('404')));

    const {getByTestId, queryAllByTestId} = render(<App />);

    const input = getByTestId('input');
    const button = getByTestId('button');

    await act(async () => {
      await fireEvent.changeText(input, 'Berlin');
      await fireEvent.press(button);
    });

    const errorComponent = getByTestId('error-component');
    const items = queryAllByTestId('list-item');

    expect(errorComponent).toBeTruthy();
    expect(items).toHaveLength(0);
  });
});
