# UCM test React Native App

This application is powered by React-Native

## Prerequisites

Before launching the app, make sure that you have a properly configured environment for Android and iOS, according to the instructions on https://reactnative.dev/docs/environment-setup

# How to start?

1.  Install dependencies

        yarn install

2.  Install CocoaPods (iOS step only)

        cd ios
        pod install

**To run app:**

    yarn ios
    yarn android

**To run test**

    yarn test

## Comment

I believe that for such a task using redux is unnecessary. Using a normal hook with state would be sufficient. I used redux and saga only because they were mentioned in the list of things to use.
