export enum FontSizes {
  XL = 30,
  Normal = 16,
  Small = 13,
  XS = 12,
  XXS = 10,
}

export enum Font {
  Roboto_Bold = 'Roboto-Bold',
  Roboto_Medium = 'Roboto-Medium',
  Roboto_Regular = 'Roboto-Regular',
  Nunito_Black = 'Nunito-Black',
}
