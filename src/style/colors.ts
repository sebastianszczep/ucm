export const theme = {
  commons: {
    primary: '#F3997B',
    background: '#FFFFFF',
    overlay: '#E6F4F1',
  },
  fonts: {
    primary: '#114358',
  },
};
