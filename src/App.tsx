import React from 'react';
import {Provider} from 'react-redux';

import MainView from './MainView';
import {buildStore} from './store/store';

const store = buildStore();

const App = () => {
  return (
    <Provider store={store}>
      <MainView />
    </Provider>
  );
};

export default App;
