import React from 'react';
import {FlatList, Image, SafeAreaView, StyleSheet} from 'react-native';
import ButtonComponent from './components/ButtonComponent';
import InputComponent from './components/InputComponent';
import ListItemComponent from './components/ListItemComponent';

import TextComponent from './components/TextComponent';
import {useFetchForecast} from './hooks/useFetchForecast';

import {WeatherInfo} from './store/types/apiDataTypes';
import {theme} from './style/colors';
import {Font, FontSizes} from './style/fonts';

const ListFooter = () => {
  const {error} = useFetchForecast();
  return (
    <>
      {error && (
        <TextComponent
          testID="error-component"
          fontFamily={Font.Roboto_Regular}
          fontSize={FontSizes.Normal}
          text="Nothing found. Please try again."
          style={styles.error}
        />
      )}
      <Image
        source={require('../assets/img/taxi-design.png')}
        style={styles.img}
      />
    </>
  );
};

const ListHeader = () => {
  const {loading, handleSubmit, handleTextChange, query} = useFetchForecast();

  return (
    <>
      <TextComponent
        text="Hello Sunshine!"
        fontFamily={Font.Nunito_Black}
        fontSize={FontSizes.XL}
        color={theme.commons.primary}
        style={styles.headerText}
      />
      <TextComponent
        text="Can you please tell me the weather in Germany?"
        fontFamily={Font.Nunito_Black}
        fontSize={FontSizes.XL}
        style={styles.headerText}
      />
      <TextComponent
        text="Please enter a city"
        fontFamily={Font.Roboto_Regular}
        fontSize={FontSizes.Normal}
        style={styles.headerText}
      />
      <InputComponent onChangeText={handleTextChange} value={query} />
      <ButtonComponent loading={loading} onPress={handleSubmit} />
    </>
  );
};

const renderItem = ({item}: {item: WeatherInfo}) => {
  return <ListItemComponent item={item} />;
};

const MainView = () => {
  const {data} = useFetchForecast();
  return (
    <SafeAreaView style={styles.container}>
      <FlatList<WeatherInfo>
        style={styles.container}
        contentContainerStyle={styles.contentContainerStyle}
        data={data}
        renderItem={renderItem}
        ListHeaderComponent={ListHeader}
        ListFooterComponent={ListFooter}
        ListFooterComponentStyle={styles.footer}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainerStyle: {
    paddingHorizontal: 20,
    paddingBottom: 320,
    flexGrow: 1,
  },
  headerContainer: {
    justifyContent: 'center',
  },
  headerText: {
    textAlign: 'center',
    marginVertical: 15,
  },
  img: {
    height: 310,
    alignSelf: 'flex-end',
    position: 'relative',
    right: -85,
    top: 10,
  },
  error: {textAlign: 'center', position: 'relative', top: -10},
  footer: {
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
  },
});

export default MainView;
