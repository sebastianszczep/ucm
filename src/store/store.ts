import {createStore, applyMiddleware, PreloadedState} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer, {RootState} from './reducers/rootReducer';
import {rootSaga} from './sagas/rootSaga';

export function buildStore(initialState?: PreloadedState<RootState>) {
  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(sagaMiddleware),
  );

  sagaMiddleware.run(rootSaga);

  return store;
}
