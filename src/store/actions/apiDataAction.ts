import {
  ApiDataActionTypes,
  FetchDataFailure,
  FetchDataFailurePayload,
  FetchDataRequest,
  FetchDataSuccess,
  FetchDataSuccessPayload,
} from '../types/apiDataTypes';

export const fetchDataRequest = (text: string): FetchDataRequest => ({
  type: ApiDataActionTypes.FETCH_DATA_REQUEST,
  text,
});

export const fetchDataSuccess = (
  payload: FetchDataSuccessPayload,
): FetchDataSuccess => ({
  type: ApiDataActionTypes.FETCH_DATA_SUCCESS,
  payload,
});

export const fetchDataFailure = (
  payload: FetchDataFailurePayload,
): FetchDataFailure => ({
  type: ApiDataActionTypes.FETCH_DATA_FAILURE,
  payload,
});
