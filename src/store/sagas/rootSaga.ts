import {all, fork} from 'redux-saga/effects';
import apiDataSaga from './apiDataSaga';

export function* rootSaga() {
  yield all([fork(apiDataSaga)]);
}
