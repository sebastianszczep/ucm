import {call, put, all, takeLatest} from 'redux-saga/effects';
import Config from '../../config';
import {fetchDataSuccess, fetchDataFailure} from '../actions/apiDataAction';
import {
  ApiDataActionTypes,
  ApiDataResponse,
  FetchDataRequest,
} from '../types/apiDataTypes';
import axios from 'axios';

const getData = (searchQuery: string) =>
  axios.get<ApiDataResponse>(`${Config.API_URL}${searchQuery},de`);

function* fetchDataSaga(action: FetchDataRequest) {
  const {text} = action;
  try {
    const response: ApiDataResponse = yield call(getData, text);
    yield put(
      fetchDataSuccess({
        data: response.data.list,
      }),
    );
  } catch (e) {
    yield put(
      fetchDataFailure({
        error: e.message,
      }),
    );
  }
}

function* apiDataSaga() {
  yield all([takeLatest(ApiDataActionTypes.FETCH_DATA_REQUEST, fetchDataSaga)]);
}

export default apiDataSaga;
