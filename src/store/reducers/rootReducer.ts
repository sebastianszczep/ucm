import {combineReducers} from 'redux';
import apiDataReducer from './apiDataReducer';

const rootReducer = combineReducers({
  apiData: apiDataReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
