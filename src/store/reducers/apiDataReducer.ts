import {
  ApiDataActionTypes,
  ApiDataState,
  DataActions,
} from '../types/apiDataTypes';

const initialState: ApiDataState = {
  loading: false,
  data: [],
};

export default (state = initialState, action: DataActions) => {
  switch (action.type) {
    case ApiDataActionTypes.FETCH_DATA_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ApiDataActionTypes.FETCH_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
        error: undefined,
      };
    case ApiDataActionTypes.FETCH_DATA_FAILURE:
      return {
        ...state,
        loading: false,
        data: [],
        error: action.payload.error,
      };
    default:
      return {
        ...state,
      };
  }
};
