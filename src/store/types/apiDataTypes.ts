export interface WeatherInfo {
  main: {
    temp: number;
    feels_like: number;
  };
  weather: {
    description: string;
  }[];
  dt: number;
}

export interface ApiDataResponse {
  data: {list: WeatherInfo[]};
}

export interface ApiDataState {
  loading: boolean;
  data: WeatherInfo[];
  error?: string;
}
export enum ApiDataActionTypes {
  FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST',
  FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS',
  FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE',
}

export interface FetchDataSuccessPayload {
  data: WeatherInfo[];
}

export interface FetchDataFailurePayload {
  error: string;
}

export interface FetchDataRequest {
  type: typeof ApiDataActionTypes.FETCH_DATA_REQUEST;
  text: string;
}

export type FetchDataSuccess = {
  type: typeof ApiDataActionTypes.FETCH_DATA_SUCCESS;
  payload: FetchDataSuccessPayload;
};

export type FetchDataFailure = {
  type: typeof ApiDataActionTypes.FETCH_DATA_FAILURE;
  payload: FetchDataFailurePayload;
};

export type DataActions =
  | FetchDataRequest
  | FetchDataSuccess
  | FetchDataFailure;
