import {useCallback, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {fetchDataRequest} from '../store/actions/apiDataAction';
import {RootState} from '../store/reducers/rootReducer';

export function useFetchForecast() {
  const dispatch = useDispatch();
  const [query, setQuery] = useState('');

  const {loading, data, error} = useSelector(
    (state: RootState) => state.apiData,
  );

  const handleSubmit = useCallback(() => {
    dispatch(fetchDataRequest(query));
  }, [query, dispatch]);

  const handleTextChange = useCallback((text: string) => {
    setQuery(text);
  }, []);

  return {handleTextChange, loading, data, error, handleSubmit, query};
}
