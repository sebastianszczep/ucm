import React, {FunctionComponent} from 'react';
import {ActivityIndicator, StyleSheet, TouchableOpacity} from 'react-native';
import {theme} from '../style/colors';
import {Font, FontSizes} from '../style/fonts';
import TextComponent from './TextComponent';

interface ButtonComponentProps {
  onPress?: () => void;
  loading?: boolean;
}

const ButtonComponent: FunctionComponent<ButtonComponentProps> = ({
  onPress,
  loading = false,
}: ButtonComponentProps) => {
  return (
    <TouchableOpacity
      testID="button"
      onPress={onPress}
      style={styles.container}
      disabled={loading}>
      {loading ? (
        <ActivityIndicator color={theme.commons.background} />
      ) : (
        <TextComponent
          text="Have a look"
          fontFamily={Font.Roboto_Bold}
          fontSize={FontSizes.Small}
          color={theme.commons.background}
          style={styles.textButton}
        />
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 160,
    height: 40,
    backgroundColor: theme.commons.primary,
    borderRadius: 3,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 25,
    marginBottom: 51,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  textButton: {
    textAlign: 'center',
  },
});
export default ButtonComponent;
