import React, {FunctionComponent, useMemo} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {WeatherInfo} from '../store/types/apiDataTypes';
import {theme} from '../style/colors';
import {Font, FontSizes} from '../style/fonts';
import TextComponent from './TextComponent';
const options = {
  weekday: 'short',
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
};

interface ListItemComponentProps {
  item: WeatherInfo;
}

const ListItemComponent: FunctionComponent<ListItemComponentProps> = ({
  item,
}: ListItemComponentProps) => {
  const {main, weather, dt} = item;

  const {date, time} = useMemo(() => {
    return {
      //@ts-ignore
      date: new Date(dt).toLocaleDateString('en-DE', options),
      time: new Date(dt).toLocaleTimeString('en-US'),
    };
  }, [dt]);

  return (
    <View style={styles.container} testID="list-item">
      <View style={[styles.row, {justifyContent: 'space-between'}]}>
        <TextComponent
          text={date}
          fontFamily={Font.Roboto_Bold}
          fontSize={FontSizes.Small}
          color={theme.commons.primary}
        />
        <TextComponent
          text={time}
          fontFamily={Font.Roboto_Bold}
          fontSize={FontSizes.XS}
          color={theme.commons.primary}
        />
      </View>
      <View style={styles.mainContent}>
        <TextComponent
          text={`temperature: ${main.temp} K`}
          fontFamily={Font.Roboto_Bold}
          fontSize={FontSizes.Normal}
        />
        <TextComponent
          text={`feel like temperature: ${main.feels_like} K`}
          fontFamily={Font.Roboto_Bold}
          fontSize={FontSizes.Normal}
          style={{lineHeight: 30}}
        />
      </View>
      <FlatList
        style={styles.row}
        renderItem={renderBubble}
        horizontal={true}
        data={weather}
      />
    </View>
  );
};

const renderBubble = ({item}: {item: {description: string}}) => {
  return (
    <View style={styles.bubble}>
      <TextComponent
        text={item.description}
        fontFamily={Font.Roboto_Bold}
        fontSize={FontSizes.XXS}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    borderWidth: 1,
    borderColor: theme.fonts.primary,
    padding: 21,
    borderRadius: 10,
    marginVertical: 10,
  },
  row: {flexDirection: 'row'},
  bubble: {
    height: 21,

    backgroundColor: theme.commons.overlay,
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 11,
  },
  mainContent: {marginTop: 7, marginBottom: 20},
});
export default ListItemComponent;
