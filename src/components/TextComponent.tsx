import React, {FunctionComponent} from 'react';
import {TextStyle, Text} from 'react-native';
import {theme} from '../style/colors';
import {Font, FontSizes} from '../style/fonts';

interface TextComponentProps {
  text: string;
  fontSize: FontSizes;
  fontFamily: Font;
  style?: Exclude<TextStyle, 'fontFamily' | 'fontSize'>;
  color?: string;
  testID?: string;
}

const TextComponent: FunctionComponent<TextComponentProps> = ({
  text,
  fontSize,
  fontFamily,
  color = theme.fonts.primary,
  style,
  testID,
}: TextComponentProps) => (
  <Text testID={testID} style={[style, {fontFamily, fontSize, color}]}>
    {text}
  </Text>
);

export default TextComponent;
