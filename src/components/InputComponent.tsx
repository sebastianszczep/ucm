import React, {FunctionComponent, useCallback, useState} from 'react';
import {TextInput, StyleSheet, TextInputProps} from 'react-native';
import {theme} from '../style/colors';
import {Font, FontSizes} from '../style/fonts';

type InputComponentProps = Exclude<TextInputProps, 'style'>;

const InputComponent: FunctionComponent<InputComponentProps> = ({
  ...props
}: InputComponentProps) => {
  const [focused, setFocused] = useState(false);

  const onBlur = useCallback(() => {
    setFocused(false);
  }, []);
  const onFocused = useCallback(() => {
    setFocused(true);
  }, []);
  return (
    <TextInput
      style={[
        styles.textInput,
        {borderColor: focused ? theme.fonts.primary : theme.commons.overlay},
      ]}
      {...props}
      testID="input"
      onBlur={onBlur}
      onFocus={onFocused}
    />
  );
};

const styles = StyleSheet.create({
  textInput: {
    borderColor: theme.fonts.primary,
    width: 234,
    height: 45,
    alignSelf: 'center',
    padding: 12,
    borderWidth: 2,
    borderRadius: 3,
    textAlign: 'center',
    fontFamily: Font.Roboto_Medium,
    fontSize: FontSizes.Normal,
    color: theme.fonts.primary,
  },
});
export default InputComponent;
